import { HttpClient, HttpHandler } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AbstractControl, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/user.service';
import { CommonModule } from '@angular/common'
import { BrowserModule } from '@angular/platform-browser'
import { PhotoUploadComponent } from './photo-upload/photo-upload.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { RegistrationComponent } from './registration.component';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
import { formatWithOptions } from 'util';
import { LoginComponent } from '../login/login.component';

describe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;
  let router: Router;
  let toastrService: ToastrService;
  let service: UserService
  let httpMock: HttpTestingController;
  let fb: FormBuilder;
  let registerSpy;
  let form: FormGroup;

  let registerFormData = {
    clientId: 3,
    username: "starjohnson",
    password: "password",
    firstName: "Star",
    lastName: "Johnson",
    address: "Space",
    age: 55,
    email: "starjohnson@maildrop.cc",
    contactNumber: "501-555-8888",
    photoUrl: null,

  }
  function updateForm() {
    const firstNameControl: AbstractControl = form.get('firstName');
    firstNameControl.setValue("juan");
    const lastNameControl: AbstractControl = form.get('lastName');
    lastNameControl.setValue("ly");
    const contactNumberControl: AbstractControl = form.get('contactNumber');
    contactNumberControl.setValue("224-333-5515");
    const addressControl: AbstractControl = form.get('address');
    addressControl.setValue("paula st");
    const usernameControl: AbstractControl = form.get('username');
    usernameControl.setValue("johnlaolaotest");
    const passwordControl: AbstractControl = form.get('password');
    passwordControl.setValue("mypassword");
    const ageControl: AbstractControl = form.get('age');
    ageControl.setValue(21);
    const emailControl: AbstractControl = form.get('email');
    emailControl.setValue("kokfe@gmail.com");
    const agreeControl: AbstractControl = form.get('agree');
    agreeControl.setValue(true);
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegistrationComponent, PhotoUploadComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule, RouterTestingModule.withRoutes([{ path: 'user/login', component: LoginComponent }]), ToastrModule.forRoot(), CommonModule, BrowserModule, BrowserAnimationsModule],
      providers: [UserService, ToastrService, FormBuilder]
    })
      .compileComponents();

    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();

    router = TestBed.inject(Router);
    service = TestBed.inject(UserService);
    toastrService = TestBed.inject(ToastrService);
    form = service.formModel;
  });

  it('should create registration component', () => {
    expect(component).toBeTruthy();
  });

  it('should have the child component', (() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('photo-upload')).not.toBe(null);
  }));

  it('should have the firstname input field', (() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#user-register-first-name-input-field')).not.toEqual(null);
  }));

  it('should not have error message', (() => {
    fixture.detectChanges();
    const errorMessage = fixture.debugElement.query(By.css('#firstNameErrorMessage'));
    expect(errorMessage).toEqual(null);
  }));


  it('should call register method upon click submit', (() => {
    fixture.detectChanges();
    updateForm();
    registerSpy = spyOn(service, 'register').and.returnValue(Observable.create(observer => {
      observer.next("resource was created");
    }));
    const button = fixture.debugElement.nativeElement.querySelector('button');
    console.log(button);
    fixture.detectChanges();
    button.click();
    expect(registerSpy).toHaveBeenCalled();

  }));

  it('should route to login if register successfully', () =>{
    updateForm();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    fixture.detectChanges();
    registerSpy = spyOn(service, 'register').and.returnValue(Observable.create(observer => {
      observer.next("resource was created");
    }))
    fixture.detectChanges();

    router = TestBed.get(Router);
    let location: Location = TestBed.get(Location);
    router.navigate(['/user/login']).then(() => {
      expect(location.path()).toBe('/user/login');
      console.log(location.path());
    })

  })

});
