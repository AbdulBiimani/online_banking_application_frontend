import { Component, ElementRef, ViewChild} from '@angular/core';
import { PhotoUploadService } from '../Services/photo-upload.service';
import { UserService } from 'src/app/shared/user.service';

import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'photo-upload',
  templateUrl: './photo-upload.component.html',
  styleUrls: ['./photo-upload.component.css']
})


export class PhotoUploadComponent {
  @ViewChild('customFile') uploadinput: ElementRef;
  // @Input() formGroupParent: FormGroup;
  // @Input() formControlNameParent: string;
  // ngOnInit() {
  //   this.formGroupParent.addControl(this.formControlNameParent, new FormControl());
  //  }
  // Name = new FormControl('');

  resetPhotoField(): void
  {
    this.uploadinput.nativeElement.value = "";
  }

  selectedFiles!: FileList;
  currentFileUpload!: File;
  fileName : String;
  progress: { percentage: number } = { percentage: 0 };
  selectedFile = null ;
  // changeImage = false;
  file !:string;
  // check
  constructor( private uploadService: PhotoUploadService, private https:HttpClient){}
  viewFile(){
    window.open('https://photo-testing1.s3.us-east-2.amazonaws.com/'+this.file);
  }

  deleteFile()
  {
    this.https.post<string>('http://localhost:9030/deleteFile',this.file).subscribe(
      res => {
        this.file = res;
      }
    );
  }
  // change(event:any) {
  //   this.changeImage = true;
  // }

  // changedImage(event:any) {
  //   this.selectedFile = event.target.files[0];
  // }
  upload() {
    this.progress.percentage = 0;
    if(this.selectedFiles != null){
      this.currentFileUpload = this.selectedFiles.item(0);
    }
    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe( ()=> {
      this.selectedFiles = undefined;
    });
    this.currentFileUpload = null;
    // this.uploadService.data = null;
    // this.uploadService.request = null;
  }

  selectFile(event:any) {
    if(event == null) {
      this.selectedFiles = null;
      this.fileName = null;
    }else{
      this.selectedFiles = event.target.files;
      // console.log(this.selectedFiles);
      this.fileName = this.selectedFiles[0].name;
    }
  }
}
