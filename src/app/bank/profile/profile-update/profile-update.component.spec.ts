import { CommonModule } from '@angular/common';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AbstractControl, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule, By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/shared/user.service';
import { ProfileComponent } from '../profile.component';

import { ProfileUpdateComponent } from './profile-update.component';

describe('RegistrationComponent', () => {
  let component: ProfileUpdateComponent;
  let fixture: ComponentFixture<ProfileUpdateComponent>
  let router: Router;
  let toastrService: ToastrService;
  let service: UserService
  let httpMock: HttpTestingController;
  let fb: FormBuilder;
  let form: FormGroup;
  let serviceSpy;


  let updateFormData = {
    clientId: 3,
    firstName: "Star",
    lastName: "Johnson",
    address: "Space",
    email: "starjohnson@maildrop.cc",
    contactNumber: "501-555-8888",
  }
  function updateForm() {
    const firstNameControl: AbstractControl = form.get('firstName');
    firstNameControl.setValue("juan");
    const lastNameControl: AbstractControl = form.get('lastName');
    lastNameControl.setValue("ly");
    const contactNumberControl: AbstractControl = form.get('contactNumber');
    contactNumberControl.setValue("224-333-5515");
    const addressControl: AbstractControl = form.get('address');
    addressControl.setValue("paula st");
    const emailControl: AbstractControl = form.get('email');
    emailControl.setValue("kokfe@gmail.com");
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProfileUpdateComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule, RouterTestingModule.withRoutes([{ path: '../viewprofile', component: ProfileComponent }]), ToastrModule.forRoot(), CommonModule, BrowserModule, BrowserAnimationsModule],
      providers: [UserService, ToastrService, FormBuilder]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ProfileUpdateComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();

    router = TestBed.inject(Router);
    service = TestBed.inject(UserService);
    toastrService = TestBed.inject(ToastrService);
    form = service.formModel;
  });

  it('should create profile update  component', () => {
    expect(component).toBeTruthy();
  });

  it('should have the firstname input field', (() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#first-name-update-input-field')).not.toEqual(null);
  }));

  it('should not have error message', (() => {
    fixture.detectChanges();
    const errorMessage = fixture.debugElement.query(By.css('#firstname-ErrorMessage'));
    expect(errorMessage).toEqual(null);
  }));

  it('should have the lastname input field', (() => {
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('#last-name-update-input-field')).not.toEqual(null);
  }));

  it('should not have error message', (() => {
    fixture.detectChanges();
    const errorMessage = fixture.debugElement.query(By.css('#lastname-ErrorMessage'));
    expect(errorMessage).toEqual(null);
  }));

 
  it('should call updateProfile function ', ()=>{
    serviceSpy = spyOn(service, `updateProfile`);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    fixture.detectChanges();
    expect(serviceSpy).toHaveBeenCalled();
  });
});

