import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DepositserviceService } from './depositservice.service';

describe('DepositserviceService', () => {
  let service: DepositserviceService;
  let fb : FormBuilder;
  let httpmock: HttpTestingController;

  const dummyDepositData ={
    account_number:"132548102",
  creditAmount:"5000",
  description:"gift"
  }
  beforeEach(() => {
    TestBed.configureTestingModule({

      imports: [ReactiveFormsModule, HttpClientTestingModule,FormsModule],   
    });
    httpmock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(DepositserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should call depositing()', ()=>{
    service.depositing(dummyDepositData.account_number, dummyDepositData.creditAmount, dummyDepositData.description).subscribe((res) =>{

    });
    const req = httpmock.expectOne('http://localhost:9030/api/transaction/deposit');
    expect(req.request.method).toBe('POST');
    req.flush(dummyDepositData);
  });
  
});
