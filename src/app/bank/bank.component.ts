import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from '../models/Client';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.css']
})
export class BankComponent implements OnInit {
  client!:Client;

  constructor(private router:Router, public service: UserService) { }

  ngOnInit(): void {
    this.service.updateToken().subscribe(res =>{
      //localStorage.setItem('token', null);
      localStorage.setItem('token', JSON.stringify(res));
      this.client =  JSON.parse(localStorage.getItem('token'));
    });
    this.client =  JSON.parse(localStorage.getItem('token'));
  }

  onLogout(){
    localStorage.removeItem('token');
    this.router.navigate(['/user/login']);
  }

}
