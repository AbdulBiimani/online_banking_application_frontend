import { AccountType } from "./AccountType";
import { Branch } from "./Branch";
import { Client } from "./Client";
import { Transaction } from "./Transaction";

export interface Account {
	accountNumber:string;
	balance:number;
	bholder:Branch;
	transList:Transaction[];
	tholder:AccountType;
}