import {Account} from 'src/app/models/Account';
export interface Client {s
	clientId:number;
	password: string;
	address:string;
	age:number;
	contactNumber:string;
	email:string;
	firstName:string;
	lastName:string;
	username:string;
	photoUrl:string;
	accList: Account[];

}